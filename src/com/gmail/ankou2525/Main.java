package com.gmail.ankou2525;

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	static String pf = ChatColor.GREEN+"[BSAT] "+ChatColor.AQUA;
	boolean bed;
	Logger log;

	public void onEnable(){
		this.getServer().getPluginManager().registerEvents(this, this);
		this.saveDefaultConfig();
		bed = true;
	}
	public void onDisable(){
	}
	@EventHandler
	public void onPI(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(!this.bed){return;}
		if(this.bed){
			if(e.getClickedBlock()==null){return;}
			if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || (e.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
				if(e.getClickedBlock().getType().equals(Material.BED_BLOCK)){
					Block b = e.getClickedBlock();
					Location loc = b.getLocation().add(0,1,0);
					getConfig().set("ベッドスポーン."+ p.getName() +".x", b.getLocation().getX());
					getConfig().set("ベッドスポーン."+ p.getName() +".y", loc.getY());
					getConfig().set("ベッドスポーン."+ p.getName() +".z", b.getLocation().getZ());
					this.saveConfig();
					p.setBedSpawnLocation(loc);
					p.sendMessage(pf + "ベッドスポーン地点を設定しました！ "+ ChatColor.GRAY +"(x"+ (int) p.getLocation().getX() +" y"+ (int) p.getLocation().getY() +" z"+ (int) p.getLocation().getZ() +")");
					p.playSound(p.getLocation(), Sound.NOTE_PIANO, 1, 1);
				}
			}
		}
	}
	@EventHandler
	public void onPM(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(!this.bed){return;}
		if(this.bed){
			if(p.getLocation().getY() < -10){
				p.setNoDamageTicks(60);
				Location loc = p.getLocation();
				loc.setX(getConfig().getDouble("ベッドスポーン."+ p.getName() +".x"));
				loc.setY(getConfig().getDouble("ベッドスポーン."+ p.getName() +".y"));
				loc.setZ(getConfig().getDouble("ベッドスポーン."+ p.getName() +".z"));
				p.teleport(loc);
				p.playSound(loc, Sound.ENDERMAN_TELEPORT, 1, 1);
				p.sendMessage(pf + "奈落に落ちたためベッドスポーンへTPしました。");
			}
		}
	}
	@EventHandler
	public void onPD(PlayerRespawnEvent e){
		Player p = e.getPlayer();
		Location loc = p.getLocation();
		loc.setX(getConfig().getDouble("ベッドスポーン."+ p.getName() +".x"));
		loc.setY(getConfig().getDouble("ベッドスポーン."+ p.getName() +".y"));
		loc.setZ(getConfig().getDouble("ベッドスポーン."+ p.getName() +".z"));
		if(this.bed){
			e.setRespawnLocation(loc);
		}
		if(!this.bed){
			e.setRespawnLocation(p.getWorld().getSpawnLocation());
		}
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, final String[] args) {
		if(cmd.getName().equalsIgnoreCase("bedhelp")){
			Player p = (Player) sender;
			p.sendMessage(pf + "/bedtp [true,false] : 奈落TP機能を有効、無効にします。");
		}
		if(cmd.getName().equalsIgnoreCase("bedtp")){
			Player p = (Player) sender;
			if(!p.isOp()){
				p.sendMessage(pf + ChatColor.DARK_RED +"あなたに権限がありません。");
				return false;
			}
			if(p.isOp()){
				if(args.length<=0){
					p.sendMessage(pf + "/bedtp [true,false]");
					return false;
				} else {
					if(args[0].equalsIgnoreCase("true")){
						this.bed = true;
						p.sendMessage(pf + "奈落に落ちた時のTP機能を有効にしました！");
					}
					if(args[0].equalsIgnoreCase("false")){
						this.bed = false;
						p.sendMessage(pf + "奈落に落ちた時のTP機能を無効にしました！");
					}
				}
			}
		}
		return bed;
	}
}